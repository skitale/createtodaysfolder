﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateTodaysFolder
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseFolder = ConfigurationManager.AppSettings["baseFolder"];
            string path = $@"{baseFolder}\{DateTime.Now.ToString("yyMMdd")}";

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}
